/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author haronsoft
 */
@Entity
@Table(name = "booking_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BookingStatus.findAll", query = "SELECT b FROM BookingStatus b"),
    @NamedQuery(name = "BookingStatus.findById", query = "SELECT b FROM BookingStatus b WHERE b.id = :id"),
    @NamedQuery(name = "BookingStatus.findByStatus", query = "SELECT b FROM BookingStatus b WHERE b.status = :status")})
public class BookingStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "status")
    private String status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transitionStatus")
    private Collection<BookingTransition> bookingTransitionCollection;

    public BookingStatus() {
    }

    public BookingStatus(Integer id) {
        this.id = id;
    }

    public BookingStatus(Integer id, String status) {
        this.id = id;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public Collection<BookingTransition> getBookingTransitionCollection() {
        return bookingTransitionCollection;
    }

    public void setBookingTransitionCollection(Collection<BookingTransition> bookingTransitionCollection) {
        this.bookingTransitionCollection = bookingTransitionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookingStatus)) {
            return false;
        }
        BookingStatus other = (BookingStatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.BookingStatus[ id=" + id + " ]";
    }
    
}
