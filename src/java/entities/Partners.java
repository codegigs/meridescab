/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author haronsoft
 */
@Entity
@Table(name = "partners")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Partners.findAll", query = "SELECT p FROM Partners p"),
    @NamedQuery(name = "Partners.findById", query = "SELECT p FROM Partners p WHERE p.id = :id"),
    @NamedQuery(name = "Partners.findByName", query = "SELECT p FROM Partners p WHERE p.name = :name"),
    @NamedQuery(name = "Partners.findByBasefare", query = "SELECT p FROM Partners p WHERE p.basefare = :basefare"),
    @NamedQuery(name = "Partners.findByKmrate", query = "SELECT p FROM Partners p WHERE p.kmrate = :kmrate"),
    @NamedQuery(name = "Partners.findByTimerate", query = "SELECT p FROM Partners p WHERE p.timerate = :timerate"),
    @NamedQuery(name = "Partners.findByActive", query = "SELECT p FROM Partners p WHERE p.active = :active"),
    @NamedQuery(name = "Partners.findByCreatedAt", query = "SELECT p FROM Partners p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "Partners.findByUpdatedAt", query = "SELECT p FROM Partners p WHERE p.updatedAt = :updatedAt")})
public class Partners implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "basefare")
    private double basefare;
    @Basic(optional = false)
    @NotNull
    @Column(name = "kmrate")
    private double kmrate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "timerate")
    private double timerate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @JoinColumn(name = "car_specific_model_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CarSpecificModel carSpecificModelId;

    public Partners() {
    }

    public Partners(Integer id) {
        this.id = id;
    }

    public Partners(Integer id, String name, double basefare, double kmrate, double timerate, boolean active, Date createdAt, Date updatedAt) {
        this.id = id;
        this.name = name;
        this.basefare = basefare;
        this.kmrate = kmrate;
        this.timerate = timerate;
        this.active = active;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBasefare() {
        return basefare;
    }

    public void setBasefare(double basefare) {
        this.basefare = basefare;
    }

    public double getKmrate() {
        return kmrate;
    }

    public void setKmrate(double kmrate) {
        this.kmrate = kmrate;
    }

    public double getTimerate() {
        return timerate;
    }

    public void setTimerate(double timerate) {
        this.timerate = timerate;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public CarSpecificModel getCarSpecificModelId() {
        return carSpecificModelId;
    }

    public void setCarSpecificModelId(CarSpecificModel carSpecificModelId) {
        this.carSpecificModelId = carSpecificModelId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partners)) {
            return false;
        }
        Partners other = (Partners) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Partners[ id=" + id + " ]";
    }
    
}
