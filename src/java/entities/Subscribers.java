/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author haronsoft
 */
@Entity
@Table(name = "subscribers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Subscribers.findAll", query = "SELECT s FROM Subscribers s"),
    @NamedQuery(name = "Subscribers.findById", query = "SELECT s FROM Subscribers s WHERE s.id = :id"),
    @NamedQuery(name = "Subscribers.findByName", query = "SELECT s FROM Subscribers s WHERE s.name = :name"),
    @NamedQuery(name = "Subscribers.findByEmail", query = "SELECT s FROM Subscribers s WHERE s.email = :email"),
    @NamedQuery(name = "Subscribers.findByApiToken", query = "SELECT s FROM Subscribers s WHERE s.apiToken = :apiToken"),
    @NamedQuery(name = "Subscribers.findByPassword", query = "SELECT s FROM Subscribers s WHERE s.password = :password"),
    @NamedQuery(name = "Subscribers.findByPhone", query = "SELECT s FROM Subscribers s WHERE s.phone = :phone"),
    @NamedQuery(name = "Subscribers.findByCountry", query = "SELECT s FROM Subscribers s WHERE s.country = :country"),
    @NamedQuery(name = "Subscribers.findByCountryCode", query = "SELECT s FROM Subscribers s WHERE s.countryCode = :countryCode"),
    @NamedQuery(name = "Subscribers.findByAuthyId", query = "SELECT s FROM Subscribers s WHERE s.authyId = :authyId"),
    @NamedQuery(name = "Subscribers.findByVerified", query = "SELECT s FROM Subscribers s WHERE s.verified = :verified"),
    @NamedQuery(name = "Subscribers.findByStatus", query = "SELECT s FROM Subscribers s WHERE s.status = :status"),
    @NamedQuery(name = "Subscribers.findByReferral", query = "SELECT s FROM Subscribers s WHERE s.referral = :referral"),
    @NamedQuery(name = "Subscribers.findByRememberToken", query = "SELECT s FROM Subscribers s WHERE s.rememberToken = :rememberToken"),
    @NamedQuery(name = "Subscribers.findByCreatedAt", query = "SELECT s FROM Subscribers s WHERE s.createdAt = :createdAt"),
    @NamedQuery(name = "Subscribers.findByUpdatedAt", query = "SELECT s FROM Subscribers s WHERE s.updatedAt = :updatedAt"),
    @NamedQuery(name = "Subscribers.findByDeviceToken", query = "SELECT s FROM Subscribers s WHERE s.deviceToken = :deviceToken")})
public class Subscribers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "name")
    private String name;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "api_token")
    private String apiToken;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "password")
    private String password;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "phone")
    private String phone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "country")
    private String country;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "country_code")
    private String countryCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "authy_id")
    private String authyId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "verified")
    private boolean verified;
    @Basic(optional = false)
    @NotNull
    @Column(name = "status")
    private int status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "referral")
    private String referral;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "remember_token")
    private String rememberToken;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "device_token")
    private String deviceToken;
    @JoinColumn(name = "accountType", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Acctype accountType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<Bookings> bookingsCollection;

    public Subscribers() {
    }

    public Subscribers(Integer id) {
        this.id = id;
    }

    public Subscribers(Integer id, String name, String email, String apiToken, String password, String phone, String country, String countryCode, String authyId, boolean verified, int status, String referral, String rememberToken, Date createdAt, Date updatedAt, String deviceToken) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.apiToken = apiToken;
        this.password = password;
        this.phone = phone;
        this.country = country;
        this.countryCode = countryCode;
        this.authyId = authyId;
        this.verified = verified;
        this.status = status;
        this.referral = referral;
        this.rememberToken = rememberToken;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deviceToken = deviceToken;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAuthyId() {
        return authyId;
    }

    public void setAuthyId(String authyId) {
        this.authyId = authyId;
    }

    public boolean getVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Acctype getAccountType() {
        return accountType;
    }

    public void setAccountType(Acctype accountType) {
        this.accountType = accountType;
    }

    @XmlTransient
    public Collection<Bookings> getBookingsCollection() {
        return bookingsCollection;
    }

    public void setBookingsCollection(Collection<Bookings> bookingsCollection) {
        this.bookingsCollection = bookingsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subscribers)) {
            return false;
        }
        Subscribers other = (Subscribers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Subscribers[ id=" + id + " ]";
    }
    
}
