/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author haronsoft
 */
@Entity
@Table(name = "acctype")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acctype.findAll", query = "SELECT a FROM Acctype a"),
    @NamedQuery(name = "Acctype.findById", query = "SELECT a FROM Acctype a WHERE a.id = :id"),
    @NamedQuery(name = "Acctype.findByAccountType", query = "SELECT a FROM Acctype a WHERE a.accountType = :accountType")})
public class Acctype implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "accountType")
    private String accountType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accountType")
    private Collection<Subscribers> subscribersCollection;

    public Acctype() {
    }

    public Acctype(Integer id) {
        this.id = id;
    }

    public Acctype(Integer id, String accountType) {
        this.id = id;
        this.accountType = accountType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    @XmlTransient
    public Collection<Subscribers> getSubscribersCollection() {
        return subscribersCollection;
    }

    public void setSubscribersCollection(Collection<Subscribers> subscribersCollection) {
        this.subscribersCollection = subscribersCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acctype)) {
            return false;
        }
        Acctype other = (Acctype) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Acctype[ id=" + id + " ]";
    }
    
}
