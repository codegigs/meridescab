/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author haronsoft
 */
@Entity
@Table(name = "transactions_type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransactionsType.findAll", query = "SELECT t FROM TransactionsType t"),
    @NamedQuery(name = "TransactionsType.findById", query = "SELECT t FROM TransactionsType t WHERE t.id = :id"),
    @NamedQuery(name = "TransactionsType.findByTransactionsType", query = "SELECT t FROM TransactionsType t WHERE t.transactionsType = :transactionsType")})
public class TransactionsType implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "transactions_type")
    private String transactionsType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "transactionsType")
    private Collection<Transactions> transactionsCollection;

    public TransactionsType() {
    }

    public TransactionsType(Integer id) {
        this.id = id;
    }

    public TransactionsType(Integer id, String transactionsType) {
        this.id = id;
        this.transactionsType = transactionsType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTransactionsType() {
        return transactionsType;
    }

    public void setTransactionsType(String transactionsType) {
        this.transactionsType = transactionsType;
    }

    @XmlTransient
    public Collection<Transactions> getTransactionsCollection() {
        return transactionsCollection;
    }

    public void setTransactionsCollection(Collection<Transactions> transactionsCollection) {
        this.transactionsCollection = transactionsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionsType)) {
            return false;
        }
        TransactionsType other = (TransactionsType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.TransactionsType[ id=" + id + " ]";
    }
    
}
