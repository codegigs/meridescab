/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author haronsoft
 */
@Entity
@Table(name = "password_resets")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PasswordResets.findAll", query = "SELECT p FROM PasswordResets p"),
    @NamedQuery(name = "PasswordResets.findById", query = "SELECT p FROM PasswordResets p WHERE p.id = :id"),
    @NamedQuery(name = "PasswordResets.findByCode", query = "SELECT p FROM PasswordResets p WHERE p.code = :code"),
    @NamedQuery(name = "PasswordResets.findByCreatedAt", query = "SELECT p FROM PasswordResets p WHERE p.createdAt = :createdAt"),
    @NamedQuery(name = "PasswordResets.findByUser", query = "SELECT p FROM PasswordResets p WHERE p.user = :user")})
public class PasswordResets implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user")
    private int user;

    public PasswordResets() {
    }

    public PasswordResets(Integer id) {
        this.id = id;
    }

    public PasswordResets(Integer id, String code, Date createdAt, int user) {
        this.id = id;
        this.code = code;
        this.createdAt = createdAt;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PasswordResets)) {
            return false;
        }
        PasswordResets other = (PasswordResets) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.PasswordResets[ id=" + id + " ]";
    }
    
}
