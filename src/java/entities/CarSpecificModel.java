/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author haronsoft
 */
@Entity
@Table(name = "car_specific_model")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CarSpecificModel.findAll", query = "SELECT c FROM CarSpecificModel c"),
    @NamedQuery(name = "CarSpecificModel.findById", query = "SELECT c FROM CarSpecificModel c WHERE c.id = :id"),
    @NamedQuery(name = "CarSpecificModel.findBySpecificModel", query = "SELECT c FROM CarSpecificModel c WHERE c.specificModel = :specificModel")})
public class CarSpecificModel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "specific_model")
    private String specificModel;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carSpecificModelId")
    private Collection<Partners> partnersCollection;
    @JoinColumn(name = "car_model_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CarModel carModelId;

    public CarSpecificModel() {
    }

    public CarSpecificModel(Integer id) {
        this.id = id;
    }

    public CarSpecificModel(Integer id, String specificModel) {
        this.id = id;
        this.specificModel = specificModel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSpecificModel() {
        return specificModel;
    }

    public void setSpecificModel(String specificModel) {
        this.specificModel = specificModel;
    }

    @XmlTransient
    public Collection<Partners> getPartnersCollection() {
        return partnersCollection;
    }

    public void setPartnersCollection(Collection<Partners> partnersCollection) {
        this.partnersCollection = partnersCollection;
    }

    public CarModel getCarModelId() {
        return carModelId;
    }

    public void setCarModelId(CarModel carModelId) {
        this.carModelId = carModelId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarSpecificModel)) {
            return false;
        }
        CarSpecificModel other = (CarSpecificModel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.CarSpecificModel[ id=" + id + " ]";
    }
    
}
