/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author haronsoft
 */
@Entity
@Table(name = "booking_transition")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BookingTransition.findAll", query = "SELECT b FROM BookingTransition b"),
    @NamedQuery(name = "BookingTransition.findById", query = "SELECT b FROM BookingTransition b WHERE b.id = :id"),
    @NamedQuery(name = "BookingTransition.findByTimeWhen", query = "SELECT b FROM BookingTransition b WHERE b.timeWhen = :timeWhen")})
public class BookingTransition implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "time_when")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeWhen;
    @JoinColumn(name = "transition_status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private BookingStatus transitionStatus;
    @JoinColumn(name = "booking", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Bookings booking;

    public BookingTransition() {
    }

    public BookingTransition(Integer id) {
        this.id = id;
    }

    public BookingTransition(Integer id, Date timeWhen) {
        this.id = id;
        this.timeWhen = timeWhen;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTimeWhen() {
        return timeWhen;
    }

    public void setTimeWhen(Date timeWhen) {
        this.timeWhen = timeWhen;
    }

    public BookingStatus getTransitionStatus() {
        return transitionStatus;
    }

    public void setTransitionStatus(BookingStatus transitionStatus) {
        this.transitionStatus = transitionStatus;
    }

    public Bookings getBooking() {
        return booking;
    }

    public void setBooking(Bookings booking) {
        this.booking = booking;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookingTransition)) {
            return false;
        }
        BookingTransition other = (BookingTransition) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.BookingTransition[ id=" + id + " ]";
    }
    
}
