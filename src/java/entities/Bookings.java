/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author haronsoft
 */
@Entity
@Table(name = "bookings")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bookings.findAll", query = "SELECT b FROM Bookings b"),
    @NamedQuery(name = "Bookings.findById", query = "SELECT b FROM Bookings b WHERE b.id = :id"),
    @NamedQuery(name = "Bookings.findByPartnerId", query = "SELECT b FROM Bookings b WHERE b.partnerId = :partnerId"),
    @NamedQuery(name = "Bookings.findByAssignedTo", query = "SELECT b FROM Bookings b WHERE b.assignedTo = :assignedTo"),
    @NamedQuery(name = "Bookings.findByPickupAddress", query = "SELECT b FROM Bookings b WHERE b.pickupAddress = :pickupAddress"),
    @NamedQuery(name = "Bookings.findByPickupCordLongitude", query = "SELECT b FROM Bookings b WHERE b.pickupCordLongitude = :pickupCordLongitude"),
    @NamedQuery(name = "Bookings.findByPickupCordLatitude", query = "SELECT b FROM Bookings b WHERE b.pickupCordLatitude = :pickupCordLatitude"),
    @NamedQuery(name = "Bookings.findByPickupTime", query = "SELECT b FROM Bookings b WHERE b.pickupTime = :pickupTime"),
    @NamedQuery(name = "Bookings.findByDestAddress", query = "SELECT b FROM Bookings b WHERE b.destAddress = :destAddress"),
    @NamedQuery(name = "Bookings.findByDestCordLongitude", query = "SELECT b FROM Bookings b WHERE b.destCordLongitude = :destCordLongitude"),
    @NamedQuery(name = "Bookings.findByDestCordLatitude", query = "SELECT b FROM Bookings b WHERE b.destCordLatitude = :destCordLatitude"),
    @NamedQuery(name = "Bookings.findByDepartureTime", query = "SELECT b FROM Bookings b WHERE b.departureTime = :departureTime"),
    @NamedQuery(name = "Bookings.findByEndTime", query = "SELECT b FROM Bookings b WHERE b.endTime = :endTime"),
    @NamedQuery(name = "Bookings.findByStatus", query = "SELECT b FROM Bookings b WHERE b.status = :status"),
    @NamedQuery(name = "Bookings.findByPromoCode", query = "SELECT b FROM Bookings b WHERE b.promoCode = :promoCode"),
    @NamedQuery(name = "Bookings.findByDistance", query = "SELECT b FROM Bookings b WHERE b.distance = :distance"),
    @NamedQuery(name = "Bookings.findByPrice", query = "SELECT b FROM Bookings b WHERE b.price = :price"),
    @NamedQuery(name = "Bookings.findByPaid", query = "SELECT b FROM Bookings b WHERE b.paid = :paid"),
    @NamedQuery(name = "Bookings.findByTransactionId", query = "SELECT b FROM Bookings b WHERE b.transactionId = :transactionId"),
    @NamedQuery(name = "Bookings.findByCreatedAt", query = "SELECT b FROM Bookings b WHERE b.createdAt = :createdAt"),
    @NamedQuery(name = "Bookings.findByUpdatedAt", query = "SELECT b FROM Bookings b WHERE b.updatedAt = :updatedAt")})
public class Bookings implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "partner_id")
    private int partnerId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "assigned_to")
    private int assignedTo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "pickup_address")
    private String pickupAddress;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pickup_cord_longitude")
    private double pickupCordLongitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pickup_cord_latitude")
    private double pickupCordLatitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pickup_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pickupTime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "dest_address")
    private String destAddress;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dest_cord_longitude")
    private double destCordLongitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dest_cord_latitude")
    private double destCordLatitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "departure_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date departureTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "promo_code")
    private String promoCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "distance")
    private double distance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    private int price;
    @Basic(optional = false)
    @NotNull
    @Column(name = "paid")
    private double paid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "transaction_id")
    private int transactionId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "booking")
    private Collection<BookingTransition> bookingTransitionCollection;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Subscribers userId;

    public Bookings() {
    }

    public Bookings(Integer id) {
        this.id = id;
    }

    public Bookings(Integer id, int partnerId, int assignedTo, String pickupAddress, double pickupCordLongitude, double pickupCordLatitude, Date pickupTime, String destAddress, double destCordLongitude, double destCordLatitude, Date departureTime, Date endTime, String status, String promoCode, double distance, int price, double paid, int transactionId, Date createdAt, Date updatedAt) {
        this.id = id;
        this.partnerId = partnerId;
        this.assignedTo = assignedTo;
        this.pickupAddress = pickupAddress;
        this.pickupCordLongitude = pickupCordLongitude;
        this.pickupCordLatitude = pickupCordLatitude;
        this.pickupTime = pickupTime;
        this.destAddress = destAddress;
        this.destCordLongitude = destCordLongitude;
        this.destCordLatitude = destCordLatitude;
        this.departureTime = departureTime;
        this.endTime = endTime;
        this.status = status;
        this.promoCode = promoCode;
        this.distance = distance;
        this.price = price;
        this.paid = paid;
        this.transactionId = transactionId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(int partnerId) {
        this.partnerId = partnerId;
    }

    public int getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(int assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public double getPickupCordLongitude() {
        return pickupCordLongitude;
    }

    public void setPickupCordLongitude(double pickupCordLongitude) {
        this.pickupCordLongitude = pickupCordLongitude;
    }

    public double getPickupCordLatitude() {
        return pickupCordLatitude;
    }

    public void setPickupCordLatitude(double pickupCordLatitude) {
        this.pickupCordLatitude = pickupCordLatitude;
    }

    public Date getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Date pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getDestAddress() {
        return destAddress;
    }

    public void setDestAddress(String destAddress) {
        this.destAddress = destAddress;
    }

    public double getDestCordLongitude() {
        return destCordLongitude;
    }

    public void setDestCordLongitude(double destCordLongitude) {
        this.destCordLongitude = destCordLongitude;
    }

    public double getDestCordLatitude() {
        return destCordLatitude;
    }

    public void setDestCordLatitude(double destCordLatitude) {
        this.destCordLatitude = destCordLatitude;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @XmlTransient
    public Collection<BookingTransition> getBookingTransitionCollection() {
        return bookingTransitionCollection;
    }

    public void setBookingTransitionCollection(Collection<BookingTransition> bookingTransitionCollection) {
        this.bookingTransitionCollection = bookingTransitionCollection;
    }

    public Subscribers getUserId() {
        return userId;
    }

    public void setUserId(Subscribers userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bookings)) {
            return false;
        }
        Bookings other = (Bookings) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Bookings[ id=" + id + " ]";
    }
    
}
