/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entities.Partners;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author haronsoft
 */
@Stateless
public class PartnersFacade extends AbstractFacade<Partners> {

    @PersistenceContext(unitName = "meridegenPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PartnersFacade() {
        super(Partners.class);
    }
    
}
