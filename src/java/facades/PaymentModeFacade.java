/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facades;

import entities.PaymentMode;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author haronsoft
 */
@Stateless
public class PaymentModeFacade extends AbstractFacade<PaymentMode> {

    @PersistenceContext(unitName = "meridegenPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PaymentModeFacade() {
        super(PaymentMode.class);
    }
    
}
